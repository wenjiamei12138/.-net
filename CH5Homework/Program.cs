﻿using System;
using System.Collections.Generic;

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
}

public class StudentManager
{
    private List<Student> students;

    public StudentManager()
    {
        students = new List<Student>();
    }

    // 增加一个学生
    public void AddStudent(Student student)
    {
        students.Add(student);
    }

    // 修改一个指定学号的学生
    public void UpdateStudent(int id, string newName)
    {
        var student = students.Find(s => s.Id == id);
        if (student != null)
        {
            student.Name = newName;
        }
    }

    // 删除一个学生
    public void DeleteStudent(int id)
    {
        var student = students.Find(s => s.Id == id);
        if (student != null)
        {
            students.Remove(student);
        }
    }

    // 查询一个指定学号的学生
    public Student FindStudent(int id)
    {
        return students.Find(s => s.Id == id);
    }

    // 遍历所有学生
    public void TraverseStudents()
    {
        foreach (var student in students)
        {
            Console.WriteLine($"学号：{student.Id}, 姓名：{student.Name}");
        }
    }
}

public class TestStudentManager
{
    public static void Main(string[] args)
    {
        StudentManager manager = new StudentManager();

        // 添加学生
        manager.AddStudent(new Student { Id = 1, Name = "张三" });
        manager.AddStudent(new Student { Id = 2, Name = "李四" });
        manager.AddStudent(new Student { Id = 3, Name = "王五" });

        // 修改学生
        manager.UpdateStudent(2, "赵六");

        // 查询学生
        var student = manager.FindStudent(1);
        Console.WriteLine($"学号：{student.Id}, 姓名：{student.Name}");

        // 遍历学生
        manager.TraverseStudents();

        // 删除学生
        manager.DeleteStudent(3);

        // 再次遍历学生
        manager.TraverseStudents();
    }
}
