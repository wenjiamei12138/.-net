﻿using System;
class Program
{
    static void Main(string[] args)
    {
        int months = 12; // 指定计算的月数
        int[] rabbits = new int[months]; // 创建一个长度为months的整数数组

        rabbits[0] = 1; // 第一个月的兔子对数为1
        rabbits[1] = 1; // 第二个月的兔子对数为1

        for (int i = 2; i < months; i++)
        {
            rabbits[i] = rabbits[i - 1] + rabbits[i - 2]; // 计算第i个月的兔子对数
        }

        for (int i = 0; i < months; i++)
        {
            Console.WriteLine($"第{i + 1}个月的兔子对数为：{rabbits[i]}"); // 输出每个月的兔子对数
        }
    }
}