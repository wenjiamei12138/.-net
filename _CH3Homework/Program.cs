﻿using System;
namespace Topic1
{
    public class Rectangle
    {
        public double length { set; get; }
        public double width { set; get; }   
        public Rectangle(double length, double width)
        {
            this.length = length;
            this.width = width;
        }   
        public double area()
        {
            return length * width;  
        }
        public double perimeter()
        {
           return  (length +width)*2;
        }
    }
    class Program
    {
        static void Main(String[] args)
        {
            double x, y;
            Console.WriteLine("请输入长方形的长：");
           bool q=double.TryParse(Console.ReadLine(), out x);
            Console.WriteLine("请输入长方形的宽:");
           bool p=double.TryParse(Console.ReadLine(), out y);
            if (p & q)
            {
                Rectangle r = new Rectangle(x, y);
                Console.WriteLine("长方形的周长为:{0}", r.perimeter());
                Console.WriteLine("长方形的面积为:{0}", r.area());
            }
        }
       
    }
}