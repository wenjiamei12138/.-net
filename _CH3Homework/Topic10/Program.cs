﻿using System;
interface IFlyable
{
    void Fly();
}
interface IQuackable
{
    void Quack();
}
interface ISwimmable
{
    void Swim();
}
class Quck : IQuackable
{
    public void Quack()
    {
        Console.WriteLine("Quack! Quack! Quack!");
    }
}
class FlyWithWings : IFlyable
{
    public void Fly()
    {
        Console.WriteLine("I can fly with wings!");
    }
}
class NoFly : IFlyable
{
    public void Fly()
    {
        Console.WriteLine("I cannot fly!");
    }
}
class NoSwim: ISwimmable
{
    public void Swim()
    {
        Console.WriteLine("I cannot swim!!!");
    }

}
class Squeak : IQuackable
{
    public void Quack()
    {
        Console.WriteLine("Squeak! Squeak! Squeak!");
    }
}
class SwimWithWings : ISwimmable
{
    public void Swim()
    {
        Console.WriteLine("I can swim with wings!");
    }
}
class SwimWithFeet : ISwimmable
{
    public void Swim()
    {
        Console.WriteLine("I can swim with my feet!");
    }
}
abstract class Duck
{
    protected IFlyable flyBehavior;
    protected IQuackable quackBehavior;
    protected ISwimmable swimBehavior;

    public Duck(IFlyable flyBehavior, IQuackable quackBehavior, ISwimmable swimBehavior)
    {
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
        this.swimBehavior = swimBehavior;
    }

    public abstract void Display();

    public void PerformFly()
    {
        flyBehavior.Fly();
    }

    public void PerformQuack()
    {
        quackBehavior.Quack();
    }

    public void PerformSwim()
    {
        swimBehavior.Swim();
    }

    public void SetFlyBehavior(IFlyable flyBehavior)
    {
        this.flyBehavior = flyBehavior;
    }

    public void SetQuackBehavior(IQuackable quackBehavior)
    {
        this.quackBehavior = quackBehavior;
    }

    public void SetSwimBehavior(ISwimmable swimBehavior)
    {
        this.swimBehavior = swimBehavior;
    }
}
class MallardDuck : Duck
{
    public MallardDuck()
        : base(new FlyWithWings(), new Quck(), new SwimWithFeet())
    {
    }

    public override void Display()
    {
        Console.WriteLine("I'm a Mallard duck.");
    }
}
class RedheadDuck : Duck
{
    public RedheadDuck()
        : base(new FlyWithWings(), new Quck(), new SwimWithFeet())
    {
    }

    public override void Display()
    {
        Console.WriteLine("I'm a Redhead duck.");
    }
}
class RubberDuck : Duck
{
    public RubberDuck()
        : base(new NoFly(), new Squeak(), new NoSwim())
    {
    }

    public override void Display()
    {
        Console.WriteLine("I'm a Rubber duck.");
    }
}
class Program
{
    static void Main(string[] args)
    {
        Duck mallardDuck = new MallardDuck();
        mallardDuck.Display();
        mallardDuck.PerformFly();
        mallardDuck.PerformQuack();
        mallardDuck.PerformSwim();

        Console.WriteLine();

        Duck redheadDuck = new RedheadDuck();
        redheadDuck.Display();
        redheadDuck.PerformFly();
        redheadDuck.PerformQuack();
        redheadDuck.PerformSwim();

        Console.WriteLine();

        Duck rubberDuck = new RubberDuck();
        rubberDuck.Display();
        rubberDuck.PerformFly();
        rubberDuck.PerformQuack();
        rubberDuck.PerformSwim();

        Console.ReadLine();
    }
}