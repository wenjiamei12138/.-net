﻿using System;
namespace Topic2
{
    class Employee
    {
        public long ID { get; set; }

        public string Name { get; set; }
        public String Sex { get; set; }
        public double Salary { set; get; }
        public Employee(long iD, string name, string sex, double salary)
        {
            ID = iD;
            Name = name;
            Sex = sex;
            Salary = salary;
        }
        public void ImproveSalary(double times)
        {
            if(times> 0)
            {
                Salary*= times;
            }
        }
        public void ImproveSalary()
        {
            Salary *= 1.1;
        }

    }

}
