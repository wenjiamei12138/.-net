﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Topic2
{
    public class appclass
    {
        public static void Main()
        {
            Employee e1 = new Employee(001, "小白", "male", 1000);
            e1.ImproveSalary();
            double s1 = e1.Salary;
            Console.WriteLine(s1.ToString());
            e1.ImproveSalary(2.0);
            double s2 = e1.Salary;  
            Console.WriteLine(s2.ToString());
        }

    }
}
