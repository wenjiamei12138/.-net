﻿using System;
class Circle
{
    public int r { get; set; }
    public double area(int rr)
    {
        return Math.PI * rr*rr;
    }
    public double perimeter(int rr)
    {
        return 2*Math.PI*rr;
    }
    
}
class Program
{
    public static void main(String[] args)
    {
         Circle circle = new Circle();
        int r = Convert.ToInt32(Console.ReadLine());
       double mj = circle.area(r);
        double zc=circle.perimeter(r);
        Console.WriteLine(mj+zc);

    }
}