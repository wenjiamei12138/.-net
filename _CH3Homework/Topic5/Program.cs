﻿using System;
class Rect
{
    public int Width { get; set; }
    public int Height { get; set; }

    public Rect(int width, int height)
    {
        Width = width;
        Height = height;
    }

    public Rect()
    {
        Width = 10;
        Height = 10;
    }

    public int Area()
    {
        return Width * Height;
    }

    public int Perimeter()
    {
        return 2 * (Width + Height);
    }
}

class PlainRect : Rect
{
    public int StartX { get; set; }
    public int StartY { get; set; }

    public PlainRect(int startX, int startY, int width, int height)
        : base(width, height)
    {
        StartX = startX;
        StartY = startY;
    }

    public PlainRect()
        : base(0, 0)
    {
        StartX = 0;
        StartY = 0;
    }

    public bool IsInside(double x, double y)
    {
        if (x >= StartX && x <= StartX + Width && y >= StartY && y <= StartY + Height)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        PlainRect plainRect = new PlainRect(10, 10, 20, 10);
        int area = plainRect.Area();
        int perimeter = plainRect.Perimeter();
        Console.WriteLine("矩形的面积为：" + area);
        Console.WriteLine("矩形的周长为：" + perimeter);
        bool isInside = plainRect.IsInside(25.5, 13);
        Console.WriteLine("点(25.5, 13)是否在矩形内：" + isInside);
        Console.ReadKey();
    }
}