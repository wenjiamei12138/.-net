﻿using System;
class Animal
{ 
    public virtual void Voice()
    {
        Console.WriteLine("动物发出了声音");
    }
}
class Cat : Animal
{
    // 重写 voice 方法
    public override void Voice()
    {
        Console.WriteLine("猫发出了“喵喵”的声音");
    }
}

// 定义 Pig 类，继承自 Animal 类
class Pig : Animal
{
    public override void Voice()
    {
        Console.WriteLine("猪发出了“哼哼”的声音");
    }
}

class Dog : Animal
{
    public override void Voice()
    {
        Console.WriteLine("狗发出了“汪汪”的声音");
    }
}

class Store
{
    public static Animal GetInstance(string animalType)
    {
        if (animalType == "dog")
        {
            return new Dog();
        }
        else if (animalType == "pig")
        {
            return new Pig();
        }
        else
        {
            return new Cat();
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Animal cat = new Cat();
        cat.Voice();
        Animal pig = new Pig();
        pig.Voice();
        Animal dog = new Dog();
        dog.Voice();
        Animal animal1 = Store.GetInstance("dog");
        animal1.Voice();
        Animal animal2 = Store.GetInstance("pig");
        animal2.Voice();
        Animal animal3 = Store.GetInstance("cat");
        animal3.Voice();
        Console.ReadKey();
    }
}