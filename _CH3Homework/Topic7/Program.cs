﻿using System;
abstract class Animal
{
    public string Name { get; set; }
    public abstract void Enjoy();
}
class Cat : Animal
{
    public string EyesColor { get; set; }
    public Cat(string name, string eyesColor)
    {
        Name = name;
        EyesColor = eyesColor;
    }

    // 实现 enjoy 方法
    public override void Enjoy()
    {
        Console.WriteLine(Name + "的猫正在引起注意，咯咯地笑起来了！");
    }
}

// 定义 Dog 类，继承自 Animal 类
class Dog : Animal
{
    // 属性 furColor
    public string FurColor { get; set; }

    // 构造方法
    public Dog(string name, string furColor)
    {
        Name = name;
        FurColor = furColor;
    }

    // 实现 enjoy 方法
    public override void Enjoy()
    {
        Console.WriteLine(Name + "的狗正在高兴地叫着“汪汪汪”！");
    }
}

// 定义 Lady 类
class Lady
{
    // 属性 name 和 pet
    public string Name { get; set; }
    public Animal Pet { get; set; }

    // 构造方法
    public Lady(string name, Animal pet)
    {
        Name = name;
        Pet = pet;
    }

    // 方法 myPetEnjoy
    public void MyPetEnjoy()
    {
        Console.Write(Name + "的宠物正在：");
        Pet.Enjoy();
    }
}

class Program
{
    static void Main(string[] args)
    {
        // 创建一个张女士，养了一只猫，让这只猫笑一笑
        Cat cat = new Cat("小花", "蓝色");
        Lady zhang = new Lady("张女士", cat);
        zhang.MyPetEnjoy();

        // 创建一个王女士，养了一只狗，让这只狗叫一叫
        Dog dog = new Dog("小黑", "黑色");
        Lady wang = new Lady("王女士", dog);
        wang.MyPetEnjoy();

        Console.ReadKey();
    }
}