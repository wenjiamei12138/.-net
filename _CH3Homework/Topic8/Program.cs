﻿/*
 8. (简答题) 按要求编写一个控制台应用程序:
（1）编写接口InterfaceA，接口中含有方法void printCapitalLetter()。
（2）编写接口InterfaceB，接口中含有方法void printLowercaseLetter()。
（3）编写非抽象类Print，该类实现了接口InterfaceA和InterfaceB。
要求printCapitalLetter()方法实现输出大写英文字母表的功能，
printLowercaseLetter()方法实现输出小写英文字母表的功能。
（4）编写测试程序，在main方法中创建Print的对象并赋值给InterfaceA的变量a，由变量a调用printCapitalLetter方法，
然后再创建Print的对象并将该对象赋值给InterfaceB的变量b，由变量b调用printLowercaseLetter方法
 */
using System;
// 定义接口 InterfaceA
interface InterfaceA
{
    void printCapitalLetter();
}

// 定义接口 InterfaceB
interface InterfaceB
{
    void printLowercaseLetter();
}

// 实现接口 InterfaceA 和 InterfaceB 的非抽象类 Print
class Print : InterfaceA, InterfaceB
{
    public void printCapitalLetter()
    {
        for (char c = 'A'; c <= 'Z'; c++)
        {
            Console.Write(c + " ");
        }
        Console.WriteLine();
    }

    public void printLowercaseLetter()
    {
        for (char c = 'a'; c <= 'z'; c++)
        {
            Console.Write(c + " ");
        }
        Console.WriteLine();
    }
}
class Program
{
    static void Main(string[] args)
    {
        // 创建 Print 的对象并赋值给 InterfaceA 的变量 a
        InterfaceA a = new Print();
        a.printCapitalLetter();

        // 创建 Print 的对象并赋值给 InterfaceB 的变量 b
        InterfaceB b = new Print();
        b.printLowercaseLetter();
        Console.ReadKey();
    }
}