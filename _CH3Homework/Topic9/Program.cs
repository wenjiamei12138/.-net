﻿using System;
interface A
{
    double area();
}
interface B
{
    void setColor(string c);
}
interface C : A, B
{
    void volume();
}
class Cylinder : C
{
    public double Radius { get; set; }
    public double Height { get; set; }
    public string Color { get; set; }

    public double area()
    {
        return Math.PI * Radius * Radius;
    }

    public void setColor(string c)
    {
        Color = c;
    }


    public void volume()
    {
        double v = area() * Height;
        Console.WriteLine("圆柱体的体积为：" + v);
    }
}
class Program
{
    static void Main(string[] args)
    {
        // 创建 Cylinder 对象
        Cylinder cylinder = new Cylinder();
        cylinder.Radius = 5;
        cylinder.Height = 10;
        cylinder.setColor("红色");

        // 调用 area 和 volume 方法
        Console.WriteLine("圆柱体的底圆面积为：" + cylinder.area());
        cylinder.volume();

        Console.ReadKey();
    }
}
