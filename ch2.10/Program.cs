﻿using System;
class Program
{
    static void Main(string[] args)
    {
        int count = 0; // 统计数量

        for (int i = 1; i <= 4; i++)
        {
            for (int j = 1; j <= 4; j++)
            {
                for (int k = 1; k <= 4; k++)
                {
                    if (i != j && j != k && i != k)
                    {
                        int num = i * 100 + j * 10 + k;
                        Console.WriteLine(num);
                        count++;
                    }
                }
            }
        }
        Console.WriteLine("能够组成的互不相同且无重复数字的三位数的数量为：" + count);
    }
}
