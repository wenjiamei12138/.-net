﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int count = 0; // 计数器，记录素数个数

        Console.WriteLine("101-200之间的素数有：");
        for (int num = 101; num <= 200; num++)
        {
            bool isPrime = true; // 标记是否为素数

            // 判断num是否为素数
            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0)
                {
                    isPrime = false; // 若能被整除，则不是素数
                    break;
                }
            }

            // 若isPrime为true，则num为素数
            if (isPrime)
            {
                Console.WriteLine(num);
                count++;
            }
        }

        Console.WriteLine($"总共有 {count} 个素数。");
    }
}