﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("所有的水仙花数为：");
        for (int num = 100; num <= 999; num++)
        {
            int digit1 = num / 100; // 获取百位数字
            int digit2 = (num / 10) % 10; // 获取十位数字
            int digit3 = num % 10; // 获取个位数字

            // 判断是否为水仙花数
            if (num == Math.Pow(digit1, 3) + Math.Pow(digit2, 3) + Math.Pow(digit3, 3))
            {
                Console.WriteLine(num);
            }
        }
    }
}