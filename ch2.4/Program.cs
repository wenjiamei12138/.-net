﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int number = 90; // 要分解质因数的正整数
        Console.Write($"{number} = ");

        for (int i = 2; i <= number; i++)
        {
            while (number % i == 0)
            {
                Console.Write(i);
                number /= i;

                if (number != 1)
                {
                    Console.Write(" * ");
                }
            }
        }
    }
}