﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入学生成绩：");
        int score = int.Parse(Console.ReadLine());

        if (score >= 90)
        {
            Console.WriteLine("成绩等级为A");
        }
        else if (score >= 60 && score <= 89)
        {
            Console.WriteLine("成绩等级为B");
        }
        else
        {
            Console.WriteLine("成绩等级为C");
        }
    }
}