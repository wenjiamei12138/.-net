﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入两个正整数m和n：");
        int m = int.Parse(Console.ReadLine());
        int n = int.Parse(Console.ReadLine());

        int gcd = GCD(m, n);
        int lcm = LCM(m, n);

        Console.WriteLine("最大公约数为：" + gcd);
        Console.WriteLine("最小公倍数为：" + lcm);
    }

    // 计算最大公约数
    static int GCD(int a, int b)
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    // 计算最小公倍数
    static int LCM(int a, int b)
    {
        return a * b / GCD(a, b);
    }
}