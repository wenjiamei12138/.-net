﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入一行字符：");
        string input = Console.ReadLine();

        int letterCount = 0;
        int spaceCount = 0;
        int digitCount = 0;
        int otherCount = 0;

        foreach (char c in input)
        {
            if (char.IsLetter(c))
            {
                letterCount++;
            }
            else if (char.IsWhiteSpace(c))
            {
                spaceCount++;
            }
            else if (char.IsDigit(c))
            {
                digitCount++;
            }
            else
            {
                otherCount++;
            }
        }

        Console.WriteLine("英文字母个数：" + letterCount);
        Console.WriteLine("空格个数：" + spaceCount);
        Console.WriteLine("数字个数：" + digitCount);
        Console.WriteLine("其他字符个数：" + otherCount);
    }
}