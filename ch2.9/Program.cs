﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int height = 100; // 初始高度
        int times = 10; // 落地次数

        int distance = 0; // 总共经过的距离
        int bounceHeight = 0; // 第10次反弹的高度

        for (int i = 0; i < times; i++)
        {
            distance += height;
            height /= 2;

            if (i == times - 1)
            {
                bounceHeight = height;
            }
        }

        Console.WriteLine("第10次落地时，共经过 {0} 米", distance);
        Console.WriteLine("第10次反弹的高度为 {0} 米", bounceHeight);
    }
}
