﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("1000以内的完数有：");

        for (int i = 1; i <= 1000; i++)
        {
            if (IsPerfectNumber(i))
            {
                Console.WriteLine(i);
            }
        }
    }

    // 判断一个数是否为完数
    static bool IsPerfectNumber(int num)
    {
        int sum = 0;

        for (int i = 1; i <= num / 2; i++)
        {
            if (num % i == 0)
            {
                sum += i;
            }
        }

        return sum == num;
    }
}