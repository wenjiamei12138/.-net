﻿using System;
using System.Collections.Generic;

// 定义学生类
public class Student
{
    public string Name { get; set; }
    public int Age { get; set; }
}

// 定义班级类
public class Class
{
    private List<Student> students;

    public Class()
    {
        students = new List<Student>();
    }

    // 添加学生到班级
    public void AddStudent(Student student)
    {
        students.Add(student);
    }

    // 按索引方式访问班级所有学生
    public Student this[int index]
    {
        get
        {
            if (index < 0 || index >= students.Count)
            {
                throw new IndexOutOfRangeException("索引超出范围");
            }
            return students[index];
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        // 创建班级对象
        Class myClass = new Class();

        // 添加学生到班级
        myClass.AddStudent(new Student { Name = "张三", Age = 18 });
        myClass.AddStudent(new Student { Name = "李四", Age = 17 });
        myClass.AddStudent(new Student { Name = "王五", Age = 16 });

        // 按索引方式访问班级所有学生
        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine($"学生姓名：{myClass[i].Name}，年龄：{myClass[i].Age}");
        }
    }
}