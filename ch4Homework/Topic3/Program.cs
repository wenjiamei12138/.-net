﻿using System;

public delegate bool GradePrint(Student s);

public class Student
{
    public GradePrint GP { set; get; }
}

public class GradeReport
{
    public static bool GradeReportOrderByTerm(Student s)
    {
        // 实现成绩单按学期排序的逻辑
        Console.WriteLine("按学期排序的成绩单：");
        return true;
    }

    public static bool GradeReportOrderBySubject(Student s)
    {
        // 实现成绩单按科目排序的逻辑
        Console.WriteLine("按科目排序的成绩单：");
        return true;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        // 创建学生对象
        Student student = new Student();

        // 设置成绩打印委托为按学期排序的成绩单
        student.GP = GradeReport.GradeReportOrderByTerm;

        // 调用成绩打印委托
        bool success = student.GP(student);

        // 检查打印是否成功
        if (success)
        {
            Console.WriteLine("成绩打印成功！");
        }
        else
        {
            Console.WriteLine("成绩打印失败！");
        }
    }
}
