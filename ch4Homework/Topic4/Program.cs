﻿using System;

// 定义气象站类
public class WeatherStation
{
    private DateTime lastUpdateTime;
    private double temperature;
    private double humidity;

    // 定义事件，当气象数据改变时触发
    public event EventHandler WeatherDataChanged;

    public DateTime LastUpdateTime
    {
        get { return lastUpdateTime; }
        set
        {
            lastUpdateTime = value;
            // 触发事件通知数据改变
            OnWeatherDataChanged();
        }
    }

    public double Temperature
    {
        get { return temperature; }
        set
        {
            temperature = value;
            // 触发事件通知数据改变
            OnWeatherDataChanged();
        }
    }

    public double Humidity
    {
        get { return humidity; }
        set
        {
            humidity = value;
            // 触发事件通知数据改变
            OnWeatherDataChanged();
        }
    }

    // 触发事件的方法
    protected virtual void OnWeatherDataChanged()
    {
        // 检查是否有订阅者，然后触发事件
        WeatherDataChanged?.Invoke(this, EventArgs.Empty);
    }
}

// 定义屏幕显示类
public class ScreenDisplay
{
    // 订阅气象数据改变事件的方法
    public void Subscribe(WeatherStation weatherStation)
    {
        weatherStation.WeatherDataChanged += OnWeatherDataChanged;
    }

    // 取消订阅气象数据改变事件的方法
    public void Unsubscribe(WeatherStation weatherStation)
    {
        weatherStation.WeatherDataChanged -= OnWeatherDataChanged;
    }

    // 处理气象数据改变事件的方法
    private void OnWeatherDataChanged(object sender, EventArgs e)
    {
        WeatherStation weatherStation = (WeatherStation)sender;
        Console.WriteLine($"屏幕显示：温度 {weatherStation.Temperature}，湿度 {weatherStation.Humidity}，最后更新时间 {weatherStation.LastUpdateTime}");
    }
}

// 定义数据库保存类
public class DatabaseSaver
{
    // 订阅气象数据改变事件的方法
    public void Subscribe(WeatherStation weatherStation)
    {
        weatherStation.WeatherDataChanged += OnWeatherDataChanged;
    }

    // 取消订阅气象数据改变事件的方法
    public void Unsubscribe(WeatherStation weatherStation)
    {
        weatherStation.WeatherDataChanged -= OnWeatherDataChanged;
    }

    // 处理气象数据改变事件的方法
    private void OnWeatherDataChanged(object sender, EventArgs e)
    {
        WeatherStation weatherStation = (WeatherStation)sender;
        Console.WriteLine($"保存气象数据：温度 {weatherStation.Temperature}，湿度 {weatherStation.Humidity}，最后更新时间 {weatherStation.LastUpdateTime}");
        // 在这里添加保存气象数据到数据库的逻辑
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        // 创建气象站对象
        WeatherStation weatherStation = new WeatherStation();

        // 创建屏幕显示对象并订阅气象数据改变事件
        ScreenDisplay screenDisplay = new ScreenDisplay();
        screenDisplay.Subscribe(weatherStation);

        // 创建数据库保存对象并订阅气象数据改变事件
        DatabaseSaver databaseSaver = new DatabaseSaver();
        databaseSaver.Subscribe(weatherStation);

        // 模拟气象数据更新
        weatherStation.Temperature = 25.5;
        weatherStation.Humidity = 60.0;
        weatherStation.LastUpdateTime = DateTime.Now;

        // 取消订阅数据库保存对象的气象数据改变事件
        databaseSaver.Unsubscribe(weatherStation);

        // 模拟气象数据更新
        weatherStation.Temperature = 26.2;
        weatherStation.Humidity = 55.0;
        weatherStation.LastUpdateTime = DateTime.Now;
    }
}