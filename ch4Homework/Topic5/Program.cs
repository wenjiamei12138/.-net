﻿
//编写一个程序，使用委托来求一个整数数组的最大值，并将最大值输出。

using System;

delegate int MaxValueDelegate(int[] nums);

class Program
{
    static int FindMaxValue(int[] nums)
    {
        int maxValue = nums[0];
        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] > maxValue)
            {
                maxValue = nums[i];
            }
        }

        return maxValue;
    }

    static void Main()
    {
        MaxValueDelegate maxDelegate = FindMaxValue;
        int[] numbers = { 5, 8, 2, 10, 3 };

        int max = maxDelegate(numbers);
        Console.WriteLine("数组中的最大值为：{0}", max);
    }
}