﻿using System;

public class MyStack<T>
{
    private T[] stackArray;
    private int top; // 栈顶指针

    public MyStack(int size)
    {
        stackArray = new T[size];
        top = -1; // 初始化栈顶指针为-1，表示栈为空
    }

    // 初始化一个栈
    public void InitStack()
    {
        top = -1;
    }

    // 清空一个栈
    public void ClearStack()
    {
        top = -1;
        Array.Clear(stackArray, 0, stackArray.Length); // 清空数组
    }

    // 入栈
    public void Push(T item)
    {
        if (top == stackArray.Length - 1)
        {
            throw new StackOverflowException("Stack is full");
        }

        top++;
        stackArray[top] = item;
    }

    // 出栈
    public T Pop()
    {
        if (top == -1)
        {
            throw new InvalidOperationException("Stack is empty");
        }

        T item = stackArray[top];
        top--;
        return item;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        MyStack<int> stack = new MyStack<int>(5);

        // 初始化栈
        stack.InitStack();

        // 入栈
        stack.Push(10);
        stack.Push(20);
        stack.Push(30);

        // 出栈
        int item1 = stack.Pop();
        int item2 = stack.Pop();

        Console.WriteLine("出栈的元素：{0}, {1}", item1, item2);
    }
}