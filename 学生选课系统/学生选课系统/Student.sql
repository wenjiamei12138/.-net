
create database Student 
CREATE TABLE Course(
	[CourseID] [int] IDENTITY(1,1) NOT NULL,      
	[CourseName] [varchar](50) NULL,
	[Cscore] [int] NULL,
	[Cteacher] [varchar](50) NULL,
	[Ctel] [varchar](11) NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Options]    
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Options](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Sid] [varchar](50) NULL,
	[CourserID] [int] NULL,
 CONSTRAINT [PK_Options] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type]   
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](50) NULL,
 CONSTRAINT [PK_Type] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userinfo]    
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userinfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Sid] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Sex] [varchar](2) NULL,
	[Tel] [varchar](11) NULL,
	[Email] [varchar](50) NULL,
	[Login] [varchar](50) NULL,
	[Pwd] [varchar](50) NULL,
	[TypeID] [int] NULL,
 CONSTRAINT [PK_Userinfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([CourseID], [CourseName], [Cscore], [Cteacher], [Ctel]) VALUES (1, N'军事理论', 2, N'李世伟', N'12748893023')
INSERT [dbo].[Course] ([CourseID], [CourseName], [Cscore], [Cteacher], [Ctel]) VALUES (2, N'戏曲鉴赏', 2, N'王海霞', N'17352637480')
INSERT [dbo].[Course] ([CourseID], [CourseName], [Cscore], [Cteacher], [Ctel]) VALUES (3, N'音乐鉴赏', 2, N'李书福', N'18394025374')
INSERT [dbo].[Course] ([CourseID], [CourseName], [Cscore], [Cteacher], [Ctel]) VALUES (1002, N'健美操', 1, N'李冰冰', N'18293048732')
INSERT [dbo].[Course] ([CourseID], [CourseName], [Cscore], [Cteacher], [Ctel]) VALUES (1003, N'啦啦操', 2, N'李商隐', N'17716315727')
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
SET IDENTITY_INSERT [dbo].[Options] ON 

INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (3, N'002', 1)
INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (1002, N'002', 2)
INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (1003, N'002', 3)
INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (1004, N'202213583920476', 1)
INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (4009, NULL, 1002)
INSERT [dbo].[Options] ([ID], [Sid], [CourserID]) VALUES (4010, NULL, 1003)
SET IDENTITY_INSERT [dbo].[Options] OFF
GO
SET IDENTITY_INSERT [dbo].[Type] ON 

INSERT [dbo].[Type] ([TypeID], [TypeName]) VALUES (1, N'学生')
INSERT [dbo].[Type] ([TypeID], [TypeName]) VALUES (2, N'管理员')
SET IDENTITY_INSERT [dbo].[Type] OFF
GO
SET IDENTITY_INSERT [dbo].[Userinfo] ON 

INSERT [dbo].[Userinfo] ([ID], [Sid], [Name], [Sex], [Tel], [Email], [Login], [Pwd], [TypeID]) VALUES (2, N'002', N'李四', N'男', N'18293647282', N'48242932@qq.com', N'654321', N'111', 1)
INSERT [dbo].[Userinfo] ([ID], [Sid], [Name], [Sex], [Tel], [Email], [Login], [Pwd], [TypeID]) VALUES (3, N'003', N'李云龙', N'男', N'15188301286', N'1770299247@qq.com', N'admin', N'111', 2)
INSERT [dbo].[Userinfo] ([ID], [Sid], [Name], [Sex], [Tel], [Email], [Login], [Pwd], [TypeID]) VALUES (5, N'202213583920476', N'张思睿', N'男', N'13583920476', N'432324111111@qq.com', N'202213583920476', N'123456', 1)
INSERT [dbo].[Userinfo] ([ID], [Sid], [Name], [Sex], [Tel], [Email], [Login], [Pwd], [TypeID]) VALUES (8, N'001', N'张三', N'男', N'1267482647', N'189483748@qq.com', N'123456', N'111', 1)
SET IDENTITY_INSERT [dbo].[Userinfo] OFF
GO
ALTER TABLE [dbo].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options_Options] FOREIGN KEY([CourserID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Options] CHECK CONSTRAINT [FK_Options_Options]
GO
ALTER TABLE [dbo].[Userinfo]  WITH CHECK ADD  CONSTRAINT [FK_Userinfo_Type] FOREIGN KEY([TypeID])
REFERENCES [dbo].[Type] ([TypeID])
GO
ALTER TABLE [dbo].[Userinfo] CHECK CONSTRAINT [FK_Userinfo_Type]
GO
ALTER TABLE [dbo].[Userinfo]  WITH CHECK ADD  CONSTRAINT [FK_Userinfo_Userinfo] FOREIGN KEY([ID])
REFERENCES [dbo].[Userinfo] ([ID])
GO
ALTER TABLE [dbo].[Userinfo] CHECK CONSTRAINT [FK_Userinfo_Userinfo]
GO
USE [master]
GO
ALTER DATABASE [Student] SET  READ_WRITE 
GO


select * from dbo.Userinfo