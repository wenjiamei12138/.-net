﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Student.Models;
using System;
using System.Linq;

namespace Student.Controllers
{
    public class StuController : Controller
    {
        StudentContext db = new StudentContext();
        /// <summary>
        /// 登录部分
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string uname, string pwd)
        {
            var info = db.Userinfos.FirstOrDefault(u => u.Login == uname && u.Pwd == pwd);
            if (info != null)
            {
                if (info.TypeId == 1)
                {
                    //获取学号
                    HttpContext.Session.SetString("sid", info.Sid);
                    //获取编号
                    HttpContext.Session.SetInt32("id", info.Id);
                    ViewBag.name = info.Name;
                    return Content("1");
                }
                if (info.TypeId == 2)
                {
                    //获取管理员登录者的名字
                    HttpContext.Session.SetString("name", info.Name);
                    ViewBag.name = info.Name;
                    return Content("2");
                }
            }
            return Content("3");
        }

        /// <summary>
        /// 以下为学生选课系统后台部分
        /// </summary>
        /// <returns></returns>
        /// 
        #region
        //管理员后台首页
        public IActionResult AdminIndex()
        {
            return View();
        }
        //学生信息管理
        public IActionResult AdminStuInfo()
        {
            var info = db.Userinfos.Where(u => u.TypeId == 1).ToList();
            ViewBag.data = info;
            return View();
        }
        //删除学生信息
        public IActionResult DelStu()
        {
            string sid = Request.Query["id"];
            var info = db.Userinfos.FirstOrDefault(u => u.Sid == sid);
            var info2 = db.Options.FirstOrDefault(u => u.Sid == sid);
            if (info2 != null)
            {
                db.Options.Remove(info2);
            }
            db.Userinfos.Remove(info);
            db.SaveChanges();
            return RedirectToAction("AdminStuInfo");
        }
        //添加学生
        public IActionResult AddStu(string name, string tel, string email, string sex)
        {
            Userinfo userinfo = new Userinfo();
            userinfo.Pwd = "123456";
            userinfo.Sid = DateTime.Now.Year.ToString() + tel;
            userinfo.Name = name;
            userinfo.Tel = tel;
            userinfo.Email = email;
            if (sex == "1")
            {
                userinfo.Sex = "男";
            }
            if (sex == "2")
            {
                userinfo.Sex = "女";
            }
            userinfo.TypeId = 1;
            userinfo.Login = DateTime.Now.Year.ToString() + tel;
            db.Userinfos.Add(userinfo);
            db.SaveChanges();
            return RedirectToAction("AdminStuInfo");
        }
        //编辑学生信息
        public IActionResult EditStu()
        {
            string sid = Request.Query["id"];
            var info = db.Userinfos.FirstOrDefault(u => u.Sid == sid);
            ViewBag.data = info;
            return View();
        }
        [HttpPost]
        public IActionResult EditStu(string sid, string name, string pwd, string tel, string email, string sex)
        {
            var info = db.Userinfos.FirstOrDefault(u => u.Sid == sid);
            info.Tel = tel;
            info.Email = email;
            info.Sex = sex;
            info.Pwd = pwd;
            info.Sex = sex;
            db.SaveChanges();
            return RedirectToAction("AdminStuInfo");
        }
        //根据学生名字进行模糊查询
        public IActionResult StuSearch(string name)
        {
            if (name != null)
            {
                var info = db.Userinfos.Where(u => u.Name.Contains(name) && u.TypeId == 1).ToList();
                ViewBag.data = info;
            }
            else
            {
                var info = db.Userinfos.Where(u => u.TypeId == 1).ToList();
                ViewBag.data = info;
            }
            return View("AdminStuInfo");
        }
        //查看管理员信息
        public IActionResult AdminInfo()
        {
            var info = db.Userinfos.Where(u => u.TypeId == 2).ToList();
            ViewBag.data = info;
            return View();
        }
        //根据管理员名字进行模糊查询
        public IActionResult AdminSearch(string name)
        {
            if (name != null)
            {
                var info = db.Userinfos.Where(u => u.Name.Contains(name) && u.TypeId == 2).ToList();
                ViewBag.data = info;
            }
            else
            {
                var info = db.Userinfos.Where(u => u.TypeId == 2).ToList();
                ViewBag.data = info;
            }
            return View("AdminInfo");
        }
        //编辑管理员信息
        public IActionResult EditAdmin()
        {
            int sid = int.Parse(Request.Query["id"]);
            var info = db.Userinfos.FirstOrDefault(u => u.Id == sid);
            ViewBag.data = info;
            return View();
        }
        [HttpPost]
        public IActionResult EditAdmin(int sid, string name, string pwd, string tel, string email, string sex)
        {
            var info = db.Userinfos.FirstOrDefault(u => u.Id == sid);
            info.Tel = tel;
            info.Email = email;
            info.Sex = sex;
            info.Pwd = pwd;
            info.Sex = sex;
            db.SaveChanges();
            return RedirectToAction("AdminInfo");
        }
        //删除管理员
        public IActionResult DelAdmin()
        {
            int sid = int.Parse(Request.Query["id"]);
            var info = db.Userinfos.FirstOrDefault(u => u.Id == sid);
            var info2 = db.Options.FirstOrDefault(u => u.Id == sid);
            if (info2 != null)
            {
                db.Options.Remove(info2);
            }
            db.Userinfos.Remove(info);
            db.SaveChanges();
            return RedirectToAction("AdminInfo");
        }
        //添加管理员
        public IActionResult AddAdmin(string name, string tel, string email, string sex)
        {
            Userinfo userinfo = new Userinfo();
            userinfo.Pwd = "123456";
            userinfo.Sid = DateTime.Now.Year.ToString() + tel;
            userinfo.Name = name;
            userinfo.Tel = tel;
            userinfo.Email = email;
            if (sex == "1")
            {
                userinfo.Sex = "男";
            }
            if (sex == "2")
            {
                userinfo.Sex = "女";
            }
            userinfo.TypeId = 2;
            userinfo.Login = DateTime.Now.Year.ToString() + tel;
            db.Userinfos.Add(userinfo);
            db.SaveChanges();
            return RedirectToAction("AdminInfo");
        }
        //查看学生选课信息
        public IActionResult StuOptions()
        {
            var info = from o in db.Options
                       join u in db.Userinfos on o.Sid equals u.Sid
                       join c in db.Courses on o.CourserId equals c.CourseId
                       select new Stuinfo()
                       {
                           Id = o.Id,
                           Name = u.Name,
                           CourseId = c.CourseId,
                           CourseName = c.CourseName,
                           Cteacher = c.Cteacher,
                           Cscore = c.Cscore
                       };
            ViewBag.data = info.ToList();
            return View();
        }
        [HttpPost]
        //按照学生名称进行已选课程查询
        public IActionResult OptionsMohu(string name)
        {
            if (name != null)
            {
                var info = from o in db.Options
                           join u in db.Userinfos on o.Sid equals u.Sid
                           join c in db.Courses on o.CourserId equals c.CourseId
                           where u.Name.Contains(name)
                           select new Stuinfo()
                           {
                               Id = o.Id,
                               Name = u.Name,
                               CourseId = c.CourseId,
                               CourseName = c.CourseName,
                               Cteacher = c.Cteacher,
                               Cscore = c.Cscore
                           };
                ViewBag.data = info.ToList();
            }
            else
            {
                var info = from o in db.Options
                           join u in db.Userinfos on o.Sid equals u.Sid
                           join c in db.Courses on o.CourserId equals c.CourseId
                           select new Stuinfo()
                           {
                               Id = o.Id,
                               Name = u.Name,
                               CourseId = c.CourseId,
                               CourseName = c.CourseName,
                               Cteacher = c.Cteacher,
                               Cscore = c.Cscore
                           };
                ViewBag.data = info.ToList();
            }
            return View("StuOptions");
        }
        //删除指定学生选课信息
        public IActionResult DelOptions()
        {
            int id = int.Parse(Request.Query["id"]);
            var info = db.Options.FirstOrDefault(o => o.Id == id);
            db.Remove(info);
            db.SaveChanges();
            return RedirectToAction("StuOptions");
        }
        //课程信息
        public IActionResult OptionsInfo()
        {
            var info = db.Courses.ToList();
            ViewBag.data = info;
            return View();
        }
        [HttpPost]
        //添加课程信息
        public IActionResult AddOptions(string ctel, string cname, string cteacher, int cscore)
        {
            Course c = new Course();
            c.CourseName = cname;
            c.Cteacher = cteacher;
            c.Cscore = cscore;
            c.Ctel = ctel;
            db.Courses.Add(c);
            db.SaveChanges();
            return RedirectToAction("OptionsInfo");
        }
        //按照课程名称进行模糊查询
        public IActionResult OptionsSearch(string cname)
        {
            var info = db.Courses.Where(c => c.CourseName.Contains(cname)).ToList();
            ViewBag.data = info;
            return View("OptionsInfo");
        }
        //课程编辑
        public IActionResult OptionsEdit()
        {
            int id = int.Parse(Request.Query["id"]);
            var info = db.Courses.FirstOrDefault(c => c.CourseId == id);
            ViewBag.data = info;
            return View();
        }
        [HttpPost]
        public IActionResult OptionsEdit(int id, string name, string teacher, int score, string tel)
        {
            var info = db.Courses.FirstOrDefault(c => c.CourseId == id);
            info.CourseName = name;
            info.Cteacher = teacher;
            info.Cscore = score;
            info.Ctel = tel;
            db.SaveChanges();
            return RedirectToAction("OptionsInfo");
        }
        #endregion
        /// <summary>
        /// 以下为前端学生选课部分
        /// </summary>
        /// <returns></returns>
        #region
        //学生首页
        public IActionResult StuIndex()
        {
            return View();
        }
        //学生个人信息
        public IActionResult MyInfo()
        {
            string sid = HttpContext.Session.GetString("sid");
            var info = db.Userinfos.FirstOrDefault(u => u.Sid == sid);
            ViewBag.data = info;
            return View();
        }
        //修改个人信息
        public IActionResult MyEdit()
        {
            string sid = HttpContext.Session.GetString("sid");
            var info = db.Userinfos.FirstOrDefault(u => u.Sid == sid);
            ViewBag.data = info;
            return View();
        }
        [HttpPost]
        public IActionResult MyEdit(string sid, string login, string name, string tel, string sex, string email, string pwd)
        {
            int id = int.Parse(HttpContext.Session.GetInt32("id").ToString());
            var info = db.Userinfos.FirstOrDefault(u => u.Id == id);
            info.Tel = tel;
            info.Email = email;
            info.Sid = sid;
            info.Login = login;
            info.Pwd = pwd;
            info.Sex = sex;
            info.Name = name;
            db.SaveChanges();
            return RedirectToAction("MyInfo");
        }
        //课程信息
        public IActionResult CourseInfo()
        {
            string sid = HttpContext.Session.GetString("sid");
            var info = db.Courses.ToList();
            ViewBag.data = info;
            ViewBag.select = sid;
            return View();
        }
        //选择课程操作
        public IActionResult SelectOptions()
        {
            int id = int.Parse(Request.Query["id"]);
            string sid = HttpContext.Session.GetString("sid");
            Option o = new Option();
            o.CourserId = id;
            o.Sid = sid;
            db.Options.Add(o);
            db.SaveChanges();
            return RedirectToAction("CourseInfo");
        }
        //对课程进行模糊查询
        public IActionResult OptionLike(string name)
        {
            if (name!=null)
            {
                string sid = HttpContext.Session.GetString("sid");
                var info = db.Courses.Where(o=>o.CourseName.Contains(name)).ToList();
                ViewBag.data = info;
                ViewBag.select = sid;
               
            }
            if (name==null)
            {
                string sid = HttpContext.Session.GetString("sid");
                var info = db.Courses.ToList();
                ViewBag.data = info;
                ViewBag.select = sid;
            }
            return View("CourseInfo");
        }
        //查看已选课程
        public IActionResult MyOptions()
        {
            string sid = HttpContext.Session.GetString("sid");
            var info = from o in db.Options
                       join u in db.Userinfos on o.Sid equals u.Sid
                       join c in db.Courses on o.CourserId equals c.CourseId
                       where o.Sid == sid
                       select new Stuinfo()
                       {
                           Id = o.Id,
                           Name = u.Name,
                           CourseId = c.CourseId,
                           CourseName = c.CourseName,
                           Cteacher = c.Cteacher,
                           Cscore = c.Cscore
                       };
            string date = "1";
            //ViewBag.date = "0";
            if (info.Count() > 0)
            {
                ViewBag.data = info.ToList();
            }
            else
            {
                ViewBag.date = date;
            }

            return View();
        }
        //取消选课
        public IActionResult DelMyOptions()
        {
            int id = int.Parse(Request.Query["id"]);
            var info = db.Options.FirstOrDefault(o => o.Id == id);
            db.Options.Remove(info);
            db.SaveChanges();
            return RedirectToAction("MyOptions");
        }
        //按照课程名称查询
        public IActionResult AlreadyOptionsMohu(string name)
        {
            if (name != null)
            {
                string sid = HttpContext.Session.GetString("sid");
                var info = from o in db.Options
                           join u in db.Userinfos on o.Sid equals u.Sid
                           join c in db.Courses on o.CourserId equals c.CourseId
                           where o.Sid == sid && c.CourseName.Contains(name)
                           select new Stuinfo()
                           {
                               Id = o.Id,
                               Name = u.Name,
                               CourseId = c.CourseId,
                               CourseName = c.CourseName,
                               Cteacher = c.Cteacher,
                               Cscore = c.Cscore
                           };

                ViewBag.data = info.ToList();
            }
            else
            {
                string sid = HttpContext.Session.GetString("sid");
                var info = from o in db.Options
                           join u in db.Userinfos on o.Sid equals u.Sid
                           join c in db.Courses on o.CourserId equals c.CourseId
                           where o.Sid == sid
                           select new Stuinfo()
                           {
                               Id = o.Id,
                               Name = u.Name,
                               CourseId = c.CourseId,
                               CourseName = c.CourseName,
                               Cteacher = c.Cteacher,
                               Cscore = c.Cscore
                           };
                ViewBag.data = info.ToList();
            }
            return View("MyOptions");
        }
        #endregion
    }
}
