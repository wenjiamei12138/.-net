﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Student.Models
{
    public partial class Course
    {
        public Course()
        {
            Options = new HashSet<Option>();
        }

        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public int? Cscore { get; set; }
        public string Cteacher { get; set; }
        public string Ctel { get; set; }

        public virtual ICollection<Option> Options { get; set; }
    }
}
