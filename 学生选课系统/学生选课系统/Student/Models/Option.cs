﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Student.Models
{
    public partial class Option
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public int? CourserId { get; set; }

        public virtual Course Courser { get; set; }
    }
}
