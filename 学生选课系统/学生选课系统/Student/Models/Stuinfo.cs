﻿namespace Student.Models
{
    public class Stuinfo
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public int? Cscore { get; set; }
        public string Cteacher { get; set; }
        public string Ctel { get; set; }
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Pwd { get; set; }
        public int? TypeId { get; set; }
    }
}
