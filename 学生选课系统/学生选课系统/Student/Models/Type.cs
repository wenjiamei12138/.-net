﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Student.Models
{
    public partial class Type
    {
        public Type()
        {
            Userinfos = new HashSet<Userinfo>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<Userinfo> Userinfos { get; set; }
    }
}
