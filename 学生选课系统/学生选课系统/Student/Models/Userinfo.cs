﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Student.Models
{
    public partial class Userinfo
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Pwd { get; set; }
        public int? TypeId { get; set; }

        public virtual Type Type { get; set; }
    }
}
